extern crate cursive_table_view;
use cursive_table_view::{TableView, TableViewItem};
use std::cmp::Ordering;

#[derive(Copy, Clone, PartialEq, Eq, Hash)]
pub enum BasicColumn {
    Name,
    Value,
}

#[derive(Clone, Debug)]
pub struct RequestHeader {
    pub name: String,
    pub value: String,
}

impl RequestHeader {
    pub fn new(name: String, value: String) -> Self {
        RequestHeader { name, value }
    }
}

impl TableViewItem<BasicColumn> for RequestHeader {
    fn to_column(&self, column: BasicColumn) -> String {
        match column {
            BasicColumn::Name => self.name.to_string(),
            BasicColumn::Value => format!("{}", self.value.to_string()),
        }
    }

    fn cmp(&self, other: &Self, column: BasicColumn) -> Ordering
    where
        Self: Sized,
    {
        match column {
            BasicColumn::Name => self.name.cmp(&other.name),
            BasicColumn::Value => self.value.cmp(&other.value),
        }
    }
}

pub fn show() -> TableView<RequestHeader, BasicColumn> {
    TableView::<RequestHeader, BasicColumn>::new()
        .column(BasicColumn::Name, "Name", |c| c.width_percent(40))
        .column(BasicColumn::Value, "Value", |c| c.width_percent(60))
}
