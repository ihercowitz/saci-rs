use cursive::traits::*;
use cursive::views::{Dialog, EditView, LinearLayout, ListView, SelectView};

pub fn panel() -> Dialog {
    let select = SelectView::new()
        .item_str("GET")
        .item_str("POST")
        .with_name("action")
        .fixed_size((8, 2));
    Dialog::new().title("Press F5 to Submit the URL").content(
        LinearLayout::horizontal()
            .child(select)
            .child(ListView::new().child("", EditView::new().with_name("url").min_width(180))),
    )
}
