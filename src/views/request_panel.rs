use super::request_header_view;
use crate::views::request_header_view::BasicColumn;
use crate::views::request_header_view::RequestHeader;
use cursive::traits::*;
use cursive::views::Checkbox;
use cursive::views::NamedView;
use cursive::views::TextView;
use cursive::views::{
    Button, Dialog, DummyView, EditView, LinearLayout, ListView, PaddedView, ResizedView, TextArea,
};
use cursive_table_view::TableView;
use cursive_tabs::{Placement, TabPanel};

fn authentication() -> PaddedView<LinearLayout> {
    PaddedView::lrtb(
        3,
        3,
        1,
        0,
        LinearLayout::vertical()
            .child(
                ListView::new()
                    .child(
                        "Username",
                        EditView::new().with_name("username").fixed_width(20),
                    )
                    .child("", DummyView)
                    .child(
                        "Password",
                        EditView::new().with_name("password").fixed_width(20),
                    )
                    .child("", DummyView),
            )
            .child(
                LinearLayout::horizontal()
                    .child(Checkbox::new().with_name("plaintext"))
                    .child(DummyView)
                    .child(TextView::new("Send as Plain Text (unsecure)")),
            ),
    )
}

fn headers() -> PaddedView<LinearLayout> {
    PaddedView::lrtb(
        3,
        3,
        1,
        0,
        LinearLayout::horizontal()
            .child(ResizedView::with_full_width(
                request_header_view::show().with_name("headers"),
            ))
            .child(DummyView)
            .child(LinearLayout::vertical().child(Button::new("Add", |s| {
                s.add_layer(
                    Dialog::around(
                        LinearLayout::vertical()
                            .child(
                                ListView::new()
                                    .child("Header", EditView::new().with_name("headerkey"))
                                    .fixed_width(30),
                            )
                            .child(
                                ListView::new()
                                    .child("Value", EditView::new().with_name("headervalue"))
                                    .fixed_width(30),
                            ),
                    )
                    .title("Add Header")
                    .button("Ok", |s| {
                        let hkey = s
                            .find_name::<EditView>("headerkey")
                            .unwrap()
                            .get_content()
                            .to_string();
                        let vkey = s
                            .find_name::<EditView>("headervalue")
                            .unwrap()
                            .get_content()
                            .to_string();

                        let mut headers = s
                            .find_name::<TableView<RequestHeader, BasicColumn>>("headers")
                            .unwrap();
                        //headers.add_item(format!("{}\t{}", hkey, vkey), vkey);
                        headers.insert_item(RequestHeader::new(hkey, vkey));
                        s.pop_layer();
                    })
                    .button("Cancel", |s| {
                        s.pop_layer();
                    }),
                )
            }))),
    )
}

fn body() -> PaddedView<NamedView<TextArea>> {
    PaddedView::lrtb(3, 3, 2, 2, TextArea::new().with_name("req_body"))
}

pub fn panel() -> TabPanel {
    TabPanel::new()
        .with_tab(authentication().with_name("Auth"))
        .with_tab(headers().with_name("Headers"))
        .with_tab(body().with_name("Body"))
        .with_bar_placement(Placement::HorizontalBottom)
        .with_active_tab("Body")
        .unwrap_or_else(|_| {
            panic!("Could not set the first tab as active tab!");
        })
}
