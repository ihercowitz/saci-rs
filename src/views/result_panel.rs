use cursive::traits::*;
use cursive::views::{DummyView, LinearLayout, ResizedView, ScrollView, TextView};

pub fn panel() -> LinearLayout {
    let response = ScrollView::new(TextView::new("").with_name("response"));

    LinearLayout::vertical().child(ResizedView::with_full_screen(
        LinearLayout::vertical()
            .child(TextView::new("").with_name("status"))
            .child(DummyView)
            .child(response),
    ))
}
