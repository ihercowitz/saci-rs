use cursive::views::{EditView, SelectView, TextArea, TextView};
use cursive::Cursive;
use cursive_tabs::TabPanel;
use reqwest::blocking::ClientBuilder;
use reqwest::{header, StatusCode};
use std::io::Read;

fn get_editview_content(s: &mut Cursive, field: String) -> String {
    s.find_name::<EditView>(&field)
        .unwrap()
        .get_content()
        .to_string()
}
fn get_authentication(s: &mut Cursive) -> String {
    let username = get_editview_content(s, "username".to_owned());
    let password = get_editview_content(s, "password".to_owned());

    match (username.is_empty(), password.is_empty()) {
        (false, false) => format!(
            "Basic {}",
            base64::encode(format!("{}:{}", username, password))
        ),
        _ => String::from(""),
    }
}

fn add_headers(s: &mut Cursive) -> header::HeaderMap {
    let authentication = get_authentication(s);

    let mut headers = header::HeaderMap::new();

    if !&authentication.is_empty() {
        let mut auth = header::HeaderValue::from_str(&authentication.clone()).unwrap();
        auth.set_sensitive(true);
        headers.insert(header::AUTHORIZATION, auth);
    }

    headers
}

fn open_response_tab(s: &mut Cursive, status: StatusCode, res: String) {
    s.call_on_name("response", |t: &mut TextView| {
        t.set_content(res);
    });

    s.call_on_name("status", |d: &mut TextView| {
        d.set_content(format!("Status {}", status))
    });

    s.call_on_name("tabitens", |t: &mut TabPanel| {
        t.set_active_tab("Response").unwrap()
    });
}

pub fn execute_request(s: &mut Cursive) {
    let url = s
        .find_name::<EditView>("url")
        .unwrap()
        .get_content()
        .to_string();

    let method = s
        .find_name::<SelectView>("action")
        .unwrap()
        .selection()
        .unwrap();

    let default_headers = add_headers(s);

    let json_body = match s.find_name::<TextArea>("req_body") {
        Some(t) => t.get_content().to_string(),
        _ => "{}".to_string(),
    };
    let client = ClientBuilder::new()
        .user_agent("SACIRS/v1")
        .build()
        .unwrap();

    let mut data = match method.as_str() {
        "GET" => client.get(url).headers(default_headers).send(),
        "POST" => client
            .post(url)
            .headers(default_headers)
            .json(&json_body)
            .send(),
        &_ => client.get(url).send(),
    };

    let res = match data {
        Ok(ref mut res) => {
            let mut body = String::new();
            res.read_to_string(&mut body).unwrap();
            body
        }

        Err(ref e) => e.to_string(),
    };

    open_response_tab(s, data.unwrap().status(), res);
}
