use cursive::event::Key;

mod actions;
mod views;

fn main() {
    let mut siv = cursive::default();
    //siv.load_toml(include_str!("assets/theme.toml")).unwrap();
    siv.add_layer(views::mainscreen());

    siv.menubar().add_leaf("Quit", |s| s.quit());
    siv.add_global_callback(Key::Esc, |s| s.select_menubar());
    siv.add_global_callback(Key::F5, |s| actions::execute_request(s));

    siv.run();
}
